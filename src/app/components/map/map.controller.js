class MapController {

  constructor($scope, uiGmapGoogleMapApi, Points, $mdDialog) {
    'ngInject';
    let vm = this;

    vm.$scope = $scope;
    vm._$mdDialog = $mdDialog;

    vm.map={
      center: { latitude: 55.75267054504671, longitude: 37.62001991271973 },
      zoom: 13,
      events: {
        center_changed: (map) => {
          Points.setCenter({
            latitude: map.center.A,
            longitude: map.center.F
          });
        }
      }
    };

    vm._Points = Points;

    vm.polyline = {
      stroke: {
        color: '#3F51B5',
        weight: 2,
        opacity: 1.0
      },
      clickable: true,
      editable: true,
      fit: true
    };

    vm.markers = Points.list();
    vm.markersUpdates = Points.getUpdates();

    vm.getPolylinePath();

    uiGmapGoogleMapApi.then((maps)=>{
      vm.geocoder = new maps.Geocoder();
      vm._gmaps = maps;
    });

    $scope.$watch(
      function watchMarkers() {
        return(Points.getUpdates());
      },
      function handleMarkersChange() {
        vm.getPolylinePath();
      }
    );
  }

  markerClick(marker) {
    let vm = this;
    marker.geoInfo = '';
    let latlng = {
      lat: marker.coords.latitude,
      lng: marker.coords.longitude
    };
    let geocode = vm.geocode(latlng);
    geocode
      .then(
        response=>{
          marker.geoInfo = response;
          vm.$scope.$apply();
        },
        error=>{
          vm._$mdDialog.show(
            vm._$mdDialog.alert()
              .parent('body')
              .clickOutsideToClose(true)
              .title("Ошибка")
              .content("Ошибка геокодера: " + error)
              .ariaLabel('Alert Dialog')
              .ok('Ok')
          );
        }
    );
  }

  geocode(latlng) {
    let vm = this;
    return new Promise((resolve, reject)=>{
      this.geocoder.geocode({'location': latlng}, function(results, status) {
        if (status === vm._gmaps.GeocoderStatus.OK) {
          if (results[1]) {
            let result = results[1].formatted_address;
            resolve(result);
          } else {
            let result = "Ничего не найдено";
            resolve(result);
          }
        } else {
          reject(status);
        }
      });
    });
  }

  getPolylinePath() {
    let path = [];
    let points = this.markers;
    points.map((point)=>{
      path.push(point.coords);
    });
    this.polyline.path = path;
  }
}

export default MapController;
