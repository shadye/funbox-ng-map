(function() {
  'use strict';

  describe('map controller', function() {

    beforeEach(module('dev'));

    var vm;
    beforeEach(inject(function(_$rootScope_, _$controller_) {
      var scope = _$rootScope_.$new();
      vm = _$controller_('MapController', {$scope: scope});
    }));

    describe('constructor', function() {


      it('map options', function() {

        expect(angular.isObject(vm.map)).toBeTruthy();
        expect(angular.isObject(vm.map.center)).toBeTruthy();
        expect(angular.isNumber(vm.map.center.latitude)).toBeTruthy();
        expect(angular.isNumber(vm.map.center.longitude)).toBeTruthy();
        expect(angular.isNumber(vm.map.zoom)).toBeTruthy();
      });

      it('declare Points factory as ctrl property', function() {
        expect(angular.isObject(vm._Points)).toBeTruthy();
      });

      it('polyline options', function() {
        expect(angular.isObject(vm.polyline)).toBeTruthy();
        expect(angular.isString(vm.polyline.stroke.color)).toBeTruthy();
        expect(angular.isNumber(vm.polyline.stroke.weight)).toBeTruthy();
        expect(angular.isNumber(vm.polyline.stroke.opacity)).toBeTruthy();
        expect(typeof vm.polyline.clickable).toEqual('boolean');
        expect(typeof vm.polyline.editable).toEqual('boolean');
        expect(typeof vm.polyline.fit).toEqual('boolean');
      });

      it('markers list init', function() {
        expect(angular.isArray(vm.markers)).toBeTruthy();
      });

      it('get number of markers list updates', function() {
        expect(angular.isNumber(vm.markersUpdates)).toBeTruthy();
      });

    });

    describe('markerClick', function() {

      var marker;
      beforeEach(function(){
        marker = {
          coords: {
            latitude: 55.7,
            longitude: 37.6
          },
          geoinfo: ''
        };
        spyOn(vm, 'markerClick');
        vm.markerClick(marker);
      });

      it('click handler call', function() {
        expect(vm.markerClick).toHaveBeenCalled();
        expect(vm.markerClick).not.toThrow();
      });

    });

    describe('geocode service', function() {

      var latlng = {
        lat: 55.7,
        lng: 37.6
      };

      it('should fetch an geocoded adress', function(){

        var testGeocode = function(result) {
          expect(angular.isString(result)).toBeTruthy();
          expect(result.length).toBeGreaterThan(0);
        };

        vm.geocode(latlng)
          .then(testGeocode)
      });

    });

    describe('rewrite polyline', function() {

      beforeEach(function(){
        spyOn(vm, 'getPolylinePath');
        vm.getPolylinePath();
      });

      it('should return array of points coords', function() {
        expect(vm.getPolylinePath).toHaveBeenCalled();
        expect(vm.getPolylinePath).not.toThrow();
      });

    });

  });
})();
