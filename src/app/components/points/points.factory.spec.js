(function() {
  'use strict';

  describe('points factory', function() {

    beforeEach(module('dev'));

    var points= [
      {
        name: "Первая точка",
        coords: {
          latitude: 55.755616894047215,
          longitude: 37.598304748535156
        },
        geoInfo: ""
      }
    ];

    var updates = 0;

    var point = {
      $$hashKey: 12,
      name: "точка",
      coords: {
        latitude: 55.755616894047215,
        longitude: 37.598304748535156
      },
      geoInfo: ""
    };

    var center = {
      latitude: 55.75267054504671,
      longitude: 37.62001991271973
    };

    var Factory;
    beforeEach(inject(function(Points) {
      Factory = Points;
    }));

    describe('points factory methods', function() {

      it('should return list of points', function() {
        expect(Factory.list()).toEqual(points);
      });

      it('should add point to list', function() {
        var tempPoints = points.slice();
        tempPoints.push(point);

        Factory.add(point);

        expect(Factory.list()).toEqual(tempPoints);
      });

      it('should remove point from list', function() {
        Factory.add(point);
        Factory.delete(point);

        expect(Factory.list()).toEqual(points);
      });

      it('should return count of list updates', function() {
        expect(Factory.getUpdates()).toEqual(updates);
      });

      it('should increase count of list updates', function() {
        Factory.update();
        updates++;

        expect(Factory.getUpdates()).toEqual(updates);
      });

      it('should return center coordinates', function() {
        expect(Factory.getCenter()).toEqual(center);
      });

      it('should set new center coordinates', function() {
        center.latitude + 2;
        center.longitude + 2;
        Factory.setCenter(center);

        expect(Factory.getCenter()).toEqual(center);
      });

    });

  });
})();
