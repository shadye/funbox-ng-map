class PointsController {
  constructor(Points) {
    'ngInject';

    this._Points = Points;
    this.points = Points.list();
  }

  addPoint() {
    this._Points.add({
      name: "",
      coords: this._Points.getCenter()
    });
  }

  onDeleteClick(ePoint) {
    this._Points.delete(ePoint);
  }

  onSort() {
    this._Points.update();
  }
}

export default PointsController;
