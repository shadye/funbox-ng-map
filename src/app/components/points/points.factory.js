let PointsFactory = ()=>{
  let points= [
    {
      name: "Первая точка",
      coords: {
        latitude: 55.755616894047215,
        longitude: 37.598304748535156
      },
      geoInfo: ""
    }
  ];
  let updates = 0;
  let center = {
    latitude: 55.75267054504671,
    longitude: 37.62001991271973
  };

  return {
    list: ()=>{
      return points;
    },
    add: (point)=>{
      points.push(point);
      updates++;
    },
    delete: (ePoint)=>{
      let index;
      points.map((point)=>{
        if (point.$$hashKey === ePoint.$$hashKey) {
          index = points.indexOf(point);
          points.splice(index, 1);
          return;
        }
      });
      updates++;
    },
    update: ()=> {
      updates++;
    },
    getUpdates: ()=> {
      return updates;
    },
    getCenter: ()=> {
      return center;
    },
    setCenter: (newCoords)=> {
      center = newCoords;
    }
  };
};

export default PointsFactory;
