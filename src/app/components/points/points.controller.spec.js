(function() {
  'use strict';

  describe('points controller', function() {

    beforeEach(module('dev'));

    var vm;
    beforeEach(inject(function(_$rootScope_, _$controller_) {
      var scope = _$rootScope_.$new();
      vm = _$controller_('PointsController', {$scope: scope});
    }));

    describe('constructor', function() {

      it('declare Points factory as ctrl property', function() {
        expect(angular.isObject(vm._Points)).toBeTruthy();
      });

      it('points list init', function() {
        expect(angular.isArray(vm.points)).toBeTruthy();
      });
    });

  })
})();
