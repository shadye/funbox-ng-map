import config from './index.config';

import runBlock from './index.run';
import PointsDirective from '../app/components/points/points.directive';
import MapDirective from '../app/components/map/map.directive';
import MapController from '../app/components/map/map.controller';
import PointsFactory from '../app/components/points/points.factory';
import PointsController from '../app/components/points/points.controller';

angular.module('dev', ['ngAnimate', 'ngMaterial', 'angular-sortable-view', 'uiGmapgoogle-maps'])
  .config(config)

  .factory('Points', PointsFactory)

  .run(runBlock)
  .controller('MapController', MapController)
  .controller('PointsController', PointsController)
  .directive('acmePoints', () => new PointsDirective())
  .directive('acmeMap', ()=> new MapDirective());
