# dev

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.12.1.

## Build & development

Run `gulp` for building, and open index.html in dist folder;
`gulp serve` for preview and follow instructions for development.

## Testing

Running `gulp test` will run the unit tests with karma.
Run `gulp` for building, and open index.html in dist folder; and `gulp serve` for preview and follow instructions.

## Testing

Running `gulp test` will run the unit tests with karma.
